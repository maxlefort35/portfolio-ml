// ------------------------ transition douce ---------------------------//
(function() 
 {
     var speed = 300;
     var moving_frequency = 15; 
     var links = document.querySelectorAll("nav a"); // Active links
     var href;
     for(var i=0; i<links.length; i++)
     {   
         href = (links[i].attributes.href === undefined) ? null : links[i].attributes.href.nodeValue.toString();
         if(href !== null && href.length > 1 && href.substr(0, 1) == '#')
         {
             links[i].onclick = function()
             {
                 var element;
                 var href = this.attributes.href.nodeValue.toString();
                 if(element = document.getElementById(href.substr(1)))
                 {
                     var hop_count = speed/moving_frequency
                     var getScrollTopDocumentAtBegin = getScrollTopDocument();
                     var gap = (getScrollTopElement(element) - getScrollTopDocumentAtBegin) / hop_count;
                     
                     for(var j = 1; j <= hop_count; j++)
                     {
                         (function()
                          {
                              var hop_top_position = gap*j;
                              setTimeout(function(){  window.scrollTo(0, hop_top_position + getScrollTopDocumentAtBegin); }, moving_frequency*j);
                          })();
                     }
                 }
                 
                 return false;
             };
         }
     }
     
     var getScrollTopElement =  function (e)
     {
         var top = 0;
         
         while (e.offsetParent != undefined && e.offsetParent != null)
         {
             top += e.offsetTop + (e.clientTop != null ? e.clientTop : 0);
             e = e.offsetParent;
         }
         
         return top;
     };
     
     var getScrollTopDocument = function()
     {
         return document.documentElement.scrollTop + document.body.scrollTop;
     };
 })();

 function myFunction() {
    var x = document.getElementById("myTopnav");
    if (x.className === "topnav") {
      x.className += " responsive";
    } else {
      x.className = "topnav";
    }
  }

  $(function(){

	var numItemsRow;
	var newNumItemsRow;
	var changeNewItemRow;
	var WichRowToAttachZoom;
	var speedOpenZoom = 500;
	var speedScroll = 500;
	var index;
	var modifIndex;
	var top;

	//ATTACH CLICK EVENT ON ALL THE IMAGES
	var tabImageGrid = document.getElementsByClassName("image-grid")
	for (var i=0 ; i<tabImageGrid.length ; i++){
		tabImageGrid[i].addEventListener("click",clickImageGrid);
	}

	function clickImageGrid(){
		//IF ZOOM ALREADY PRESENT, CLOSE IT
		if($("#container-zoom").length > 0){
			closeZoom(this);
			return;
		}

		//GET INDEX OF IMAGE CLICKED
		index = defineIndex($(this));

		//GET LINK TO BIG IMAGE
		var linkImageBig = this.getElementsByTagName('img')[0].getAttribute("data-big")
		numItemsRow = NumberRow($(".image-grid").width());

		var newTarget = findFirstElementRow();

		//ATTACH AND LOAD THE ZOOM
		$("<div id='container-zoom'><div id='zoom'><div id='overlay-close' class='buttonMode'><div class='overlay-close-x'>X</div></div></div><div id='bubble-tail'></div></div>").insertBefore(newTarget);
		LoadNewImage( document.getElementById("zoom"), linkImageBig);
		document.getElementById("overlay-close").addEventListener("click", closeZoom);
	}



	function LoadNewImage(target, url) {
	   	//SHOW LOADING SCREEN
	   	$("#loading-overlay").css("display","block");
	   	$('#loading-overlay').animate({ opacity: 0.5 },{ duration: 200, easing: 'easeInOutQuint', 
			complete: function () {
				//LOAD NEW IMAGE
			    var newImage = new Image();
			    newImage.src = url;
			    newImage.onload = function() {
			    	var zoomMaxHeight = newImage.height;
			    	var zoomMaxWidth = newImage.width;
			        // image is loaded into browser memory, so will display instantly
			        $(target).css({"background-image": 'url(' + url + ')', "max-height": "100%", "max-width": "100%"});
			        openZoom(zoomMaxHeight);
			        //REMOVE OVERLAY LOADING
			        $('#loading-overlay').animate({ opacity: 0 },{ duration: 200, easing: 'easeInOutQuint',
			        	complete:function(){
			        		$(this).css("display", "none");
			        	}
			        });
			    }
			}
		});
	}

	function defineIndex(target){
		var index = target.index();
		return index;
	}	

	function defineTarget(Index, NumItemsRow){
		modifIndex = Index;
		var posLeftImage = $(".image-grid").eq(modifIndex).position().left;
		if(posLeftImage > 5){
			//IF IMAGE IS NOT FIRST IN ROW, GET PREVIOUS IMAGE
			modifIndex--
			defineTarget(modifIndex, NumItemsRow);
		}else{
			//IF IMAGE IS FIRST IN ROW, RETURN ITS INDEX
			return modifIndex;
		}
	}

	function NumberRow(Width){
		var widthElement = Width;
		var widthContainer = $("#grid").width();
		var numberItemsRow = Math.round(widthContainer / widthElement);
		return numberItemsRow;
	}

	function openZoom(Height){
		//POSITION TAIL BUBBLE
		positionBubble();

		$("#container-zoom").css("display","block");
		$("#container-zoom").animate({ "height" : Height-30, "padding": "20px", "margin": "5px" },{duration: speedOpenZoom, easing: 'easeInOutQuint',});

		var heightScrollTarget = $("#container-zoom").position().top -5;
		scrollToTarget(heightScrollTarget);
	}

	function closeZoom(newTarget){
		document.getElementById("overlay-close").removeEventListener("click", closeZoom);
		$("#container-zoom").animate({ "height": 0, "padding-top": 0, "padding-bottom": 0, "margin-top": 0,"margin-bottom": 0},{duration: speedOpenZoom, easing: 'easeInOutQuint',
			complete:function(){
				$("#container-zoom").remove();
				$(newTarget).trigger('click');
			}
		});
	}

	function positionBubble(){
		var posBubble = $(".image-grid").eq(index).position().left;
		var sizeImage = $(".image-grid").eq(index).width();
		var middleBubble = (sizeImage/2)-($("#bubble-tail").width()/2)

		$("#bubble-tail").css("left", posBubble);
		$("#bubble-tail").css("margin-left", middleBubble);
	}

	function findFirstElementRow(){
		//DEFINE WHERE THE ROW BEGINS AND TARGET IT
		defineTarget(index, numItemsRow);
		var target = $(".image-grid").eq(modifIndex);
		return target;
	}

	function resize(){
		//IF ZOOM PRESENT
		if($("#container-zoom").length > 0){
			positionBubble();

			var newTarget = findFirstElementRow();

			//MOVE THE ZOOM TO THE BEGINNING OF THE ROW OF THE CURRENT ITEM
			$("#container-zoom").insertBefore(newTarget);
		}
	}


	function scrollToTarget(target){
		$("body").animate({ "scrollTop": target },{ duration: speedScroll, easing: 'easeInOutQuint', });
	}

	window.onresize=resize;

	//LOADING ICON
	var cl = new CanvasLoader('canvasloader-container');
	cl.setColor('#ffffff'); // default is '#000000'
	cl.setShape('spiral'); // default is 'oval'
	cl.setDiameter(47); // default is 40
	cl.setDensity(121); // default is 40
	cl.setRange(1); // default is 1.3
	cl.setSpeed(5); // default is 2
	cl.setFPS(60); // default is 24
	cl.show(); // Hidden by default

	//IE FALLBACK FOR OBJECT FIT
	if ( ! Modernizr.objectfit ) {
	  $('.image-grid').each(function () {
	  	console.log("ok");
	    var $container = $(this),
	        imgUrl = $container.find('img').prop('src');
	    if (imgUrl) {
	    	$(".image-grid").children("img").css("opacity",0);
	    	$container
	        	.css({'backgroundImage': 'url(' + imgUrl + ')'})
	        	.addClass('compat-object-fit');
	    }  
	  });
	}


});

